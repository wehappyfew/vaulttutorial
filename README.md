# Chapter 1

## Introduction

In this course, you will learn about **Vault** and experiment on it using **Minikube**.  
At first, we will play with a simple dev server in order to learn the basics and feel comfortable, but we will swiftly deploy a production server.  
Next, we will examine how we can fetch credentials from the Vault server and provide them to a sample python application using [consul-template](https://github.com/hashicorp/consul-template) and [envconsul](https://github.com/hashicorp/envconsul). This way, you will afterwards be capable of migrating all your credentials out of your applications.  
Have no illusions. Secret management is a tricky subject. We will discuss a lot of concepts and create a working flow, however there is some underlying theory that needs to be studied. The official docs are an invaluable companion.

## Vault

### What is Vault?

> Vault is a tool for securely accessing secrets.  
A secret is anything that you want to tightly control access to, such as API keys, passwords, or certificates.  
Vault provides a unified interface to any secret, while providing tight access control and recording a detailed audit log.  

[Here is the official documentation](https://www.vaultproject.io/docs/what-is-vault/index.html)

## Minikube

### What is Minikube?

> Minikube is a tool that makes it easy to run Kubernetes locally.  
Minikube runs a single-node Kubernetes cluster inside a Virtual Machine (VM) on your laptop for users looking to try out Kubernetes or develop with it day-to-day.

### Install Minikube

The installation process of Minikube is out of the scope of this course.  
[Here](https://kubernetes.io/docs/tasks/tools/install-minikube/) you can find all the necessary intructions to install Minikube according to your platform.

### Start Minikube

After you have successfully installed Minikube, open a terminal and execute this command:  

> `minikube start`  

The VM that is created by default has the following specs:  
*CPUs=2, Memory=2048MB, Disk=20000MB*

When the creation of the cluster is complete, you are going to see an output similar to this:

```bash
o   minikube v1.0.1 on windows (amd64)
$   Downloading Kubernetes v1.14.1 images in the background ...
>   Creating virtualbox VM (CPUs=2, Memory=2048MB, Disk=20000MB) ...
-   "minikube" IP address is 192.168.99.107
o   Found network options:
    - NO_PROXY=192.168.99.101,192.168.99.105,192.168.99.100,192.168.99.102
-   Configuring Docker as the container runtime ...
    - env NO_PROXY=192.168.99.101,192.168.99.105,192.168.99.100,192.168.99.102
-   Version of container runtime is 18.06.3-ce
:   Waiting for image downloads to complete ...
-   Preparing Kubernetes environment ...
-   Pulling images required by Kubernetes v1.14.1 ...
-   Launching Kubernetes v1.14.1 using kubeadm ...
:   Waiting for pods: apiserver proxy etcd scheduler controller dns
-   Configuring cluster permissions ...
-   Verifying component health .....
+   kubectl is now configured to use "minikube"
=   Done! Thank you for using minikube!
```

If you execute `minikube status` you should see the following output.

```text
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

### Install Vault

The installation process for Vault is really smooth. You can follow the official guide [here](https://www.vaultproject.io/docs/install/).

## Use Vault in development mode

Create a dev server (dev server is **ONLY** for experimentation, **NEVER** for production)

> `vault server -dev`

You will see an output somewhat like the following

```bash
==> Vault server configuration:

             Api Address: http://127.0.0.1:8200
                     Cgo: disabled
         Cluster Address: https://127.0.0.1:8201
              Listener 1: tcp (addr: "127.0.0.1:8200", cluster address: "127.0.0.1:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
               Log Level: info
                   Mlock: supported: false, enabled: false
                 Storage: inmem
                 Version: Vault v1.1.2
             Version Sha: 0082501623c0b704b87b1fbc84c2d725994bac54

WARNING! dev mode is enabled! In this mode, Vault runs entirely in-memory
and starts unsealed with a single unseal key. The root token is already
authenticated to the CLI, so you can immediately begin using Vault.

You may need to set the following environment variable:

PowerShell:
    $env:VAULT_ADDR="http://127.0.0.1:8200"
cmd.exe:
    set VAULT_ADDR=http://127.0.0.1:8200

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

Unseal Key: 3t0DVUez0WNxNcuYkvPyRsUWpJmoKj9Amiq69YEPaHM=
Root Token: s.KvSAiDqilqJh4HQPF1UI7FjC

Development mode should NOT be used in production installations!

==> Vault server started! Log data will stream in below:

...
```

This will run a Vault server on `localhost:8200`.  Open it in a browser. You see that you need a token to login.

Check the terminal where you started the server. There is the token in the output.

**Pro Tip:**  
In the beginning, while experimenting with Vault, you will most probably destroy and recreate it many times.  
Copy-Pasting the random computer generated root key is tiresome.  
To avoid it, use this command :  

> `vault server -dev -dev-root-token-id="root"` 

> The development server starts unsealed ([check here what this means](https://www.vaultproject.io/docs/concepts/seal.html)) to make your life easier.  
> A production setup will need to be unsealed.

## Start Vault in production mode

**Step 1**

Create a config file (eg config.hcl). [Check here a basic one](https://www.vaultproject.io/docs/configuration/index.html).

```json
storage "file" {
path = "./vault_storage"
}
ui = true
disable_mlock = true
api_addr = "http://127.0.0.1:8200"
listener "tcp" {
address = "127.0.0.1:8200"
tls_disable = 1
}
```
  
> For simplicity, the above configuration uses a file as backend storage.  
> A real production setup, would use AWS S3, Consul etc.

Start the server with this command :  

> `vault server -config ./vault_setup/config.hcl`

You'll most probably get something like this:

```bash
==> Vault server configuration:

Api Address: http://127.0.0.1:8200
Cgo: disabled
Cluster Address: https://127.0.0.1:8201
Listener 1: tcp (addr: "127.0.0.1:8200", cluster address: "127.0.0.1:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
Log Level: info
Mlock: supported: false, enabled: false
Storage: file
Version: Vault v1.1.2
Version Sha: 0082501623c0b704b87b1fbc84c2d725994bac54

==> Vault server started! Log data will stream in below:

2019-06-28T15:17:19.586+0300 [INFO] core: security barrier not initialized
2019-06-28T15:17:19.586+0300 [INFO] core: seal configuration missing, not initialized
```

You can see that the Vault server started, but it is not initialized and it is sealed.

**Step 2**

Now, let's initialize the Vault.

If you go to `localhost:8200`, you'll see this.

![vault-initilize](./images/v1.png)

For now (*and only for now*) enter **1** in both fields. It will make your life easier while you play with Vault.  

> Vault uses an algorithm known as [Shamir's Secret Sharing](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing) to split the master key into shards.  
These shards (or key pieces) would then be distributed to a number of operators (e.g the DevOps Lead, the Chied Sec Officer, the CTO etc).  
A certain threshold of shards is required to reconstruct the master key. The operators that have one piece/shard of this key, would need to come together and unseal the Vault.  
Imagine something like the [Two-man rule](https://en.wikipedia.org/wiki/Two-man_rule) used in nuclear submarines. 
![2man-rule](./images/two-man-rule.jpg)  
(For more info about this step [check here](https://www.vaultproject.io/docs/concepts/seal.html).)

After the successful initilization, you will be prompted to download the file that is created.

![vault-creds](./images/v2.png)

Save it somewhere and open it in your IDE.

It will be similar to this.

```json
{
  "keys": [
    "ba43ff3b9ff41a5536955e1efc7da504f1e0aa6812ede539adc93b68731dbd4b"
  ],
  "keys_base64": [
    "ukP/O5/0GlU2lV4e/H2lBPHgqmgS7eU5rck7aHMdvUs="
  ],
  "root_token": "s.GnWv1hZ4TlYkHwXGEbb2edWi"
}
```

Now, use the master key (the "keys" value) you just saved and unseal the Vault.

![unseal](./images/vault_unseal.png)

Next, insert the root token (the "root_token" value) in order to sign in.

![signin](./images/vault_signin.png)

> You just signed in from the UI (this means the API), not the terminal (cli).  
In order to login from the terminal execute `vault login <TOKEN>`

So, let's recap. 
- we started a Vault server (in prod mode),  
- initialised it (got the master key/s and root token),  
- unsealed it with the master key, and signed in with root privileges.

> Let's see a graphical representation of the unsealing process
![vault](./images/vault.jpg)

> If you browse the folder you are working in, you will notice a folder called `vault_storage`. 
> This is the folder that is used for backend storage.
> If you want to exercise in initializing and unsealing the Vault, you need to change the backend storage file name.
> Or just delete the folder that is created. Restarting the Vault server does nothing to an already setup up Vault.

**Step 3**

Set the Vault address as environmental variable in your terminal

> `export VAULT_ADDR='http://127.0.0.1:8200'`

Login to the Vault as root from the terminal (aka via the cli)

> `vault login <TOKEN>`

You'll get an output like this:

```bash
λ vault login s.yVnMWZpjOJJRZFDYArVAbP9x
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key Value
--- -----
token s.yVnMWZpjOJJRZFDYArVAbP9x
token_accessor nH5FyK0VXDMHLe6HHFCPdnH3
token_duration ∞
token_renewable false
token_policies ["root"]
identity_policies []
policies ["root"]
```

## Enable Kubernetes Auth Method

#### Run this script  

TODO --->>>>>>> Na e3igisw ti akrivws kanei!!!!! <<<<<<<<<<---

> `bash ./vault_setup/setup-k8s-auth.sh`

> Troubleshooting:  
> If you get a `* permission denied` kind of error, you must run `vault login <root_token>`.

Now, let's test the connection with Vault from inside the cluster (Minikube in our case)

- launch a simple pod, named `tmp`
    > `kubectl run --generator=run-pod/v1 tmp --rm -i --tty --serviceaccount=vault-auth --image alpine:3.7`

- update and install *curl* & *jq* inside the tmp pod
    > `apk update && apk add curl jq`

> To access a resource, such as Vault service running on your host, from inside of a pod inside Minikube, you need to get the bridge IP.
> The service running on the host machine (Vault in our case), needs to be bound to all IP’s (0.0.0.0) and interfaces in order to be reachable. 
> If is bound only to localhost (127.0.0.1), this will not work.
> This is why we have set Vault listener's in `config.hcl` to `address = "0.0.0.0:8200"`. 
> Run this command `minikube ssh "route -n | grep ^0.0.0.0 | awk '{ print \$2 }'"` and use the IP you got.
> You will most probably get back `10.0.2.2`.

- The IP `10.0.2.2` is the IP the Vault server runs on. For Minikube is the default IP of the host.  
Be sure to set VAULT_ADDR to where your Vault server is running if it's not running locally.
    > `VAULT_ADDR=http://10.0.2.2:8200`

- Check the Vault server status
    > `curl -s $VAULT_ADDR/v1/sys/health | jq`

    The output must be similar to this
    ```
    {
    "initialized": true,
    "sealed": false,
    "standby": false,
    "performance_standby": false,
    "replication_performance_mode": "disabled",
    "replication_dr_mode": "disabled",
    "server_time_utc": 1563892609,
    "version": "1.1.2",
    "cluster_name": "vault-cluster-8258e637",
    "cluster_id": "b43992c2-42a6-1409-2e81-d534b72567f5"
    }
    ```

- Test the kubernetes auth method to ensure that you can authenticate with Vault
    
    > KUBE_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

    > curl --request POST --data '{"jwt": "'"$KUBE_TOKEN"'", "role": "vault_role"}' $VAULT_ADDR/v1/auth/kubernetes/login | jq

    The output must be similar to this
    ```
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100  1555  1{0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    "request_id": "f559c053-31df-f1c4-e548-45e27deee8ca",
    "lease_id": "",
    "renewable": false,
    "lease_duration": 0,
    "data": null,
    "wrap_info": null,
    "warnings": null,
    00     "auth": {
        "client_token": "s.30hEPUKx5IHd9hS8Fwv88VgY",
        "accessor": "vhzCsXtjiRCeYXcA2smkr6MN",
        "policies": [
        "app-kv-ro",
        "default"
        ],
        "token_policies": [
        "app-kv-ro",
        "default"
        ],
        "metadata": {
        "role": "vault_role",
        "service_account_name": "vault-auth",
        "service_account_namespace": "default",
    6      "service_account_secret_name": "vault-auth-token-9n7pj",
    6      "service_account_uid": "289c68a6-ad57-11e9-b8c6-080027d25cd3"
        },
        "lease_duration": 86400,
        "renewable": true,
        "entity_id": "2e410147-e9ac-c393-dc53-b77f27d90741",
        "token_type": "service",
        "orphan": true
    }
    8  1}
    00   887  12603  16735 --:--:-- --:--:-- --:--:-- 29339
    ```

You can read more [in the Vault docs](https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s).

So, let's recap.  

- we started a Vault server (production mode), initialized & unsealed it

- we enabled the Kubernetes Authentication method on Vault

In the next chapter, we will examine how to pass the credentials 

- by writing them in a file using **concul-template**

- as environmental variables using **envconsul**

# Chapter 2

## Auto-authenticate with Vault & get a client token 

So, the Kubernetes auth method has been configured on the Vault server.
Now, it is time to spin up a pod which leverages *Vault Agent* to automatically authenticate with Vault and retrieve a client token.

### Create the ------ ConfigMap (**vault-agent-config**)

> `kubectl create configmap vault-cm --from-file=./vault-configs/`

### Create a config map to hold the VAULT_ADDR value

When we want to pass dynamically environmental variables to a pod, we can use a config map.  
In this case, we create the `vault-addr-cm` config map in order to pass inside the pod the bridge ip which can use to communicate with the Vault. 

> `kubectl create configmap vault-addr-cm --from-literal=VAULT_ADDR=http://[bridge-ip]:8200`

### Launch the pod  

> `kubectl apply -f pod.yml`

### Launch the Kubernetes dashboard to check the pod

> `minikube dashboard`

Oh bummer, you stare at the kubernetes dashboard, and all you can see is an error!

![ErrImageNeverPull-error](./images/v3.png)

Fear not! It is really simple actually.  
In the `pod.yml` file, we have instructed Kubernetes to use an image called `myappimg`. Obviously this is not an official image found in a registry like Docker Hub.  
This is why we need to build it.  In order to do this, you have to have Docker installed.  
But, before building the image we need to switch to the Minikube VM.

> `eval $(minikube docker-env)`

> The command `minikube docker-env` returns a set of Bash environment variable exports to configure your local environment to re-use the Docker daemon inside the Minikube instance.

Now, let's build the image.  

> `docker build -t myappimg .`

When the build process finishes successfully, we can go back to the Kubernetes dashboard.  
The image was found and the pod is deployed. Yay!

![pod-deployed](./images/v4.png)

For now, we are only interested in deploying the pod successfully. Next, we will dig deeper.

You can read more [in the Vault docs.](https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s#step-4-leverage-vault-agent-auto-auth)

## Case Study 1 : consul-template

In this section we will see how to fetch credentials that are stored in Vault, and feed them dynamically in an application container to use them.
Using **consul-template** we can write the credentials in a file inside our app's container.

*All the files discussed here reside in [this repo](https://wehappyfew@bitbucket.org/wehappyfew/vaultpython.git) on **consul-template** branch.*

Prerequisites:

- An initialised and unsealed Vault server.

- Minikube

We have already deployed a super simple Python application. The only thing that it does is to print the credentials.

This is it's Dockerfile.

```dockerfile
FROM python:2.7
RUN apt update -y && apt install -y nano
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python","myapp.py"]
```

As discussed earlier, we have built the docker image `myappimg`.

The deployed pod contains 3 (yes 3!) containers. 

- one that handles the authentication with the Vault server and gets the auth token (**vault-agent-auth**), 

- one that uses this auth token and fetches the credentials from Vault (**consul-template**), 

- and finally the application container (**python**). 

![3-containers](./images/v5.png)

The `~/vault-configs/consul-template-config.hcl` file, is the configuration file for the consul-template container.

```json
vault {
    renew_token = false
    vault_agent_token_file = "/home/vault/.vault-token"
    retry {
        ackoff = "1s"
    }
}
template {
    destination = "app/secrets/creds.txt"
    contents = <<EOH
    {{- with secret "secret/app/config" }}
    username = "{{ .Data.username }}"
    password = "{{ .Data.password }}"
    {{ end }}
    EOH
}
```

In the **vault** part, consul-template uses the auth token that the vault-agent-auth container obtained, in order to be able to have access to Vault.

In the **template** part, creates a new file at the **destination** path with the **contents** that we set. In this case, it fetches the username/password key/value pairs from the **secret/app/config** endpoint of the `secrets` secrets engine.

The *app/secrets/* folder is a volume that is mounted on the pod, and is shared between the consul-template container and the application container.

### Lets' check it!

Open the Minikube dashboard and check the logs of the containers. This way you will get a taste of what each one does.

Forward the app's port 5000 to your localhost's 8080 with  

> `kubectl port-forward pod/sample 8080:5000` 

Now, open in your browser `localhost:8080` . 

Voila  !!!! 

The secrets have propagated from Vault to your application! Yay!

### Auto update

Consul-template will update dynamically the secret any time it changes in Vault.

In order to do so, the secret has to be created with a **ttl** value. 

The update will happen in half of the lease time. So if the `ttl=30`, the update will happen in 15 seconds.

Go ahead and try it.  

Open Vault and edit the password. 

![vault_secrets](./images/vault_secrets.png)

Save the change, and go to `localhost:8080`.  

![vault_changed_password](./images/vault_changed_password.png)  

Wait 15 seconds and hit refresh a couple of times.  
Kaboom! Neat ha ?!

## Case Study 2 : envconsul

Similarly to the previous section, we will fetch credentials that are stored in Vault, and feed them dynamically in an application container to use them.
Using **envconsul** we can pass the credentials inside our app's container as environmental variables.

*All the files discussed here reside in [this repo](https://wehappyfew@bitbucket.org/wehappyfew/vaultpython.git) on **envconsul** branch.*

Prerequisites:

- An initialised and unsealed Vault server.

- Minikube

In this case, we need to make some changes to the Python app's Dockerfile.  
We need to add the installation of **envconsul**.

This is the new Dockerfile.

```dockerfile
FROM python:2.7

# for debugging
# RUN apt update -y && apt install -y nano

# install envconsul
RUN wget https://releases.hashicorp.com/envconsul/0.8.0/envconsul_0.8.0_linux_amd64.zip&&unzip envconsul_0.8.0_linux_amd64.zip\
    && ln -sf $PWD/envconsul /usr/local/bin

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 5000

# 1. envconsul loads the config file
# 2. fetches the creds from Vault and writes them as ENV vars
# 3. the python app starts running
ENTRYPOINT envconsul -config ./vault-configs/envconsul.hcl python myapp.py
```

Like before, 

- Switch to the Minikube's environment 
    > `eval $(minikube docker-env)`

- Build the image.
    > `docker build -t myappimg .`

> If you previously created the pod with the **consul-template** configuration you need to delete the pod and the config map.

Run 

- `kubectl delete pod sample`

- `kubectl delete cm vault-cm`

Now, to work with **envconsul**

- create the config map
    > `kubectl create configmap vault-cm --from-file=./vault-configs/`

- create the pod 
    > `kubectl apply -f pod.yml`  

This pod contains 2 containers.  

- one that handles the authentication with the Vault server and gets the auth token (**vault-agent-auth**), 

- and the application container (**python**).

The app container starts with envconsul using the auth token and fetching the credentials from Vault.  

The `~/vaultpython/vault-configs/envconsul.hcl` file, is the configuration file that is fed to the envconsul binary (you can check this at the last line of the Dockerfile).
```json

# the settings to connect to Vault server
# "http://10.0.2.2:8200" is the Vault's address on the host machine when using Minikube
vault {
  renew_token = false
  retry {
    backoff = "1s"
  }
}
# the settings to find the endpoint of the secrets engine
secret {
    no_prefix = true
    path = "secret/app/config"
}
```

In the **vault** part , envconsul uses the auth token that the vault-agent-auth container obtained, in order to be able to have access to Vault. The Vault address and auth token are used from env vars.

In the **secret** part, it fetches the username/password key/value pairs from the **secret/app/config** endpoint of the `secrets` secrets engine.

### Lets' check it!

Open the Minikube dashboard and check the logs of the containers.

Forward the app's port 5000 to your localhost's 8080  with `kubectl port-forward pod/sample 8080:5000` .

Open in your browser `localhost:8080` . 

Voila  !!!! 

The secrets have propagated from Vault to your application!

**Pro Tip:**  
If you execute `kubectl exec -it sample -c python env` from a terminal, you will get all the environmental variables of the application container.  
You will notice that our credentials are not there.  
However, the application knows and uses them. Cool right?

### Auto update

Envconsul, just like consul-template, will update dynamically the secret any time it changes in Vault.
Go ahead and check it, the process is exactly the same.

## Troubleshooting

Q: I run `sh ./vault_setup/setup-k8s-auth.sh` and I get a ton of `"Permission denied"` errors.  
A: You haven't logged in. Run `vault login <YOUR_TOKEN>` .

Q: I get the error `"Error reading secret/app/config: Get https://127.0.0.1:8200/v1/secret/app/config: http: server gave HTTP response to HTTPS client"`.  
A: Make sure that you have set up the env var `VAULT_ADDR=http://127.0.0.1:8200` correctly.

Q: How can I make sure that the container talks to the Vault server?  
A: Easy.   
- Get into a running pod  
`kubectl exec -it <pod_name> -c <container_name> bash` (or `sh`, depending on the container)  
- Set the `VAULT_ADDR` env var  
`export VAULT_ADDR=http://10.0.2.2:8200` (or the IP/PORT that your Vault server listens to)  
- Make a curl call  
`curl -s $VAULT_ADDR/v1/sys/health | jq`  

Q: I get an error `"Container image "myappimg" is not present with pull policy of Never Error: ErrImageNeverPull"` in the Kubernetes dashboard.  
A: The image cannot be found because most probably it is not build yet. Open a terminal, go to the project path, and execute `eval $(minikube docker-env)` followed by `docker build -t myappimg .`

## Resources:

- https://www.hashicorp.com/resources/adopting-hashicorp-vault

- https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s#overview

- https://learn.hashicorp.com/vault/developer/sm-app-integration

- https://github.com/hashicorp/envconsul

- https://github.com/hashicorp/consul-template