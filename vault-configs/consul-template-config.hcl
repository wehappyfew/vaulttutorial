vault {
  renew_token = false
  vault_agent_token_file = "/home/vault/.vault-token"
  retry {
    backoff = "1s"
  }
}

template {
  # where we want the file to be placed
  destination = "app/secrets/creds.txt"
  # what we want to be written in the file
  contents = <<EOH
  {{- with secret "secret/app/config" }}
  username = "{{ .Data.username }}"
  password = "{{ .Data.password }}"
  {{ end }}
  EOH
}
