from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    # open the file
    f = open("secrets/creds.txt", "r")
    # get all the contents
    text = f.read()
    # show them in the browser
    return text

if __name__ == '__main__':
    app.run(host='0.0.0.0') #debug=True,