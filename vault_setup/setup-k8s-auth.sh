#!/bin/bash

# Create a service account, 'vault-auth'
kubectl create serviceaccount vault-auth

# Update the 'vault-auth' service account
kubectl apply --filename ./vault_setup/vault-auth-service-account.yml

# Create the config map
kubectl create configmap vault-cm --from-file=./vault-configs/

# Set VAULT_SA_NAME to the service account you created earlier
export VAULT_SA_NAME=$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")
echo "VAULT_SA_NAME: " ${VAULT_SA_NAME}
echo
# Set SA_JWT_TOKEN value to the service account JWT used to access the TokenReview API
export SA_JWT_TOKEN=$(kubectl get secret ${VAULT_SA_NAME} -o jsonpath="{.data.token}" | base64 -d; echo)
echo "SA_JWT_TOKEN: " ${SA_JWT_TOKEN}
echo
# Set SA_CA_CRT to the PEM encoded CA cert used to talk to Kubernetes API
export SA_CA_CRT=$(kubectl get secret ${VAULT_SA_NAME} -o jsonpath="{.data['ca\.crt']}" | base64 -d; echo)
echo "SA_CA_CRT: " ${SA_CA_CRT}
echo
# Set K8S_HOST to minikube IP address
export K8S_HOST=$(minikube ip)
echo "K8S_HOST: " ${K8S_HOST}
echo

SECRETS_ENGINE_NAME="secret"

# Create a policy file, app-kv-ro.hcl
# This assumes that the Vault server is running kv v1 (non-versioned kv)
tee ./vault_setup/app-kv-ro.hcl <<EOF
# For K/V v1 secrets engine
path "${SECRETS_ENGINE_NAME}/app/*" {
    capabilities = ["read", "list"]
}

# For K/V v2 secrets engine
path "${SECRETS_ENGINE_NAME}/data/app/*" {
    capabilities = ["read", "list"]
}
EOF

# Create a read-only policy
vault policy write app-kv-ro ./vault_setup/app-kv-ro.hcl

# Enable the Kubernetes auth method at the default path ("auth/kubernetes")
vault auth enable kubernetes

# Tell Vault how to communicate with the Kubernetes cluster
vault write auth/kubernetes/config \
    token_reviewer_jwt="${SA_JWT_TOKEN}" \
    kubernetes_host="https://${K8S_HOST}:8443" \
    kubernetes_ca_cert="${SA_CA_CRT}"

# Create a role to map Kubernetes Service Account to Vault policies and default token TTL
ROLE_NAME="vault_role"
vault write auth/kubernetes/role/${ROLE_NAME} \
    bound_service_account_names=vault-auth \
    bound_service_account_namespaces=default \
    policies=app-kv-ro \
    ttl=24h

### === testing === ###
    USERNAME='kabamaru'
    PASSWORD='suP3rsec(et!'
    # Enable K/V v1 at ${SECRETS_ENGINE_NAME}/ if it's not already available
    vault secrets enable -path=${SECRETS_ENGINE_NAME} kv
    # Create test data in the `${SECRETS_ENGINE_NAME}/app` path.
    vault kv put ${SECRETS_ENGINE_NAME}/app/config username=${USERNAME} password=${PASSWORD} ttl='30s'
    # Enable userpass auth method
    vault auth enable userpass
    # Create a user
    vault write \
        auth/userpass/users/${USERNAME} \
        password=${PASSWORD} policies=app-kv-ro
    # Login as the user
    vault login \
        -method=userpass \
        username=${USERNAME} \
        password=${PASSWORD}
    # Test to see if the user can read ${SECRETS_ENGINE_NAME}/app path as policy has written
    vault kv get ${SECRETS_ENGINE_NAME}/app/config
### === testing === ###