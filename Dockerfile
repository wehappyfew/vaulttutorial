FROM python:2.7
RUN apt update -y && apt install -y nano

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python","myapp.py"]
